//Kanvardeep Rakhraj 20622622
//Sally Hui 20608550
#include "a3_avl_tree.hpp"	
#include <iostream>
// Overriden insert and remove functions 
// Do not modify these definitions

using namespace std;

bool AVLTree::insert(BinarySearchTree::DataType val) {
	BinarySearchTree::insert(val);
	balanceAVLTree(val);
	return true;
}

bool AVLTree::remove(DataType val){
	BinarySearchTree::Node* traverse = BinarySearchTree::getRootNode();
	BinarySearchTree::Node* backNode = BinarySearchTree::getRootNode();
	bool end = false;
	while(traverse&&!end){
	 	if (traverse != backNode)
	 		backNode = traverse;
	 	if (traverse->left)
	 	{
	 		if (traverse->left->val == val)
	 			end = true;
		}
		if (traverse->right)
	 	{
	 		if (traverse->right->val == val)
	 			end = true;
		}
	 	if(val<traverse->val)
	 		traverse = traverse->left;
	 	else 
	 		traverse = traverse->right;
	}
	BinarySearchTree::remove(val);
	balanceAVLTree(backNode->val);
	return true;
}

int AVLTree::getDepth(BinarySearchTree::Node* node){
	if (!node)
		return -1;
	else
		return 1+fmax(AVLTree::getDepth(node->left),AVLTree::getDepth(node->right));
}

// Define additional functions and attributes that you need below

AVLTree::BinarySearchTree::Node* AVLTree::getParent(BinarySearchTree::Node* node){
	BinarySearchTree::Node* traverse = BinarySearchTree::getRootNode();
	BinarySearchTree::Node* backNode = BinarySearchTree::getRootNode();
	bool end = false;
	
	while(traverse&&!end){
	 	if (traverse != backNode)
	 		backNode = traverse;
	 		
	 		
	 	if (traverse->left)
	 	{
	 		if (traverse->left->val == node->val)
	 			end = true;
		}
		
		if (traverse->right)
	 	{
	 		if (traverse->right->val == node->val)
	 			end = true;
		}
		
	 	if(node->val<traverse->val)
	 		traverse = traverse->left;
	 	else 
	 		traverse = traverse->right;
		}
	return backNode;
}

int AVLTree::getNodeDepth(BinarySearchTree::Node* n){
	int ctr = 0;
	bool end = false;
	BinarySearchTree::Node* node = BinarySearchTree::getRootNode();
	while (node != n && node){
		if (n->val < node->val)
			node =  node->left;
		else if (n->val >= node->val)
			node = node->right;
		ctr++;
	}
	return ctr;
}

void AVLTree::balanceAVLTree(BinarySearchTree::DataType val){
	if (!BinarySearchTree::getRootNode())
		return;
	BinarySearchTree::Node* node = BinarySearchTree::getRootNode();
	while(node->val != val){
		if (node->val < val)
			node = node->right;
		else
			node = node->left;
	}
	BinarySearchTree::Node* parent = AVLTree::getParent(node);
	bool end = false;

	AVLTree::updateNodeBalance();
	while (parent && !end){
		if (node == BinarySearchTree::getRootNode()->left || node == BinarySearchTree::getRootNode()->right)
			end = true;
		if (node == BinarySearchTree::getRootNode())
		{		
			if (!node->left && !node->right)
				return;
			else if (node->left && !node->right)
			{
				if (node->avlBalance == 2) 
					AVLTree::doubleLR(node, BinarySearchTree::getRootNode());
				else
					return;
			}
			else if (node->right && !node->left)
			{
				if (node->avlBalance == -2)
					AVLTree::singleLeft(node, BinarySearchTree::getRootNode());
				else
					return;
			}
			else if (node->left)
			{
				if (node->avlBalance == 1 && node->left->avlBalance == -1)
					AVLTree::doubleLR(node, BinarySearchTree::getRootNode());
			}
			else if (node->right)
			{
				return;
				if (node->avlBalance == 1 && node->left->avlBalance == -1)
					AVLTree::doubleLR(node, BinarySearchTree::getRootNode());
			}
		}
		else if (parent->avlBalance==-1 && AVLTree::getParent(parent)->avlBalance <= -2)
			AVLTree::singleLeft(getParent(parent), getParent(getParent(parent)));
		else if (parent->avlBalance>0 && AVLTree::getParent(parent)->avlBalance >= 2)
			AVLTree::singleRight(getParent(parent), getParent(getParent(parent)));
		else if (parent->avlBalance<0 && AVLTree::getParent(parent)->avlBalance >= 2)
			AVLTree::doubleLR(getParent(parent), getParent(getParent(parent)));
		else if (parent->avlBalance>0 && AVLTree::getParent(parent)->avlBalance <= -2)
			AVLTree::doubleRL(getParent(parent), getParent(getParent(parent)));
		else if (node->left)
		{
			if (node->left->left)
			{
				if (node->avlBalance == 2 && node->left->avlBalance == 1)
					AVLTree::singleRight(node, getParent(node));
			}
		}
		else if (node->right)
		{
			if (node->right->right)
			{
				if (node->avlBalance == -2 && node->left->avlBalance == -1)
					AVLTree::singleRight(node, getParent(node));
			}
		}
		node = parent;
		parent = AVLTree::getParent(parent);
		AVLTree::updateNodeBalance();
	}
}

void AVLTree::singleRight(BinarySearchTree::Node* node, BinarySearchTree::Node* parent){
	if (!node || !parent)
		return;
	if (BinarySearchTree::getRootNode() == node){
		BinarySearchTree::Node* templeft = BinarySearchTree::getRootNode()->left;
		BinarySearchTree::Node* temp = BinarySearchTree::getRootNode()->left->right;
		BinarySearchTree::getRootNode()->left->right = BinarySearchTree::getRootNode();
		BinarySearchTree::getRootNode()->left = temp;
		*BinarySearchTree::getRootNodeAddress() = templeft;
	}
	else
	{
		BinarySearchTree::Node* q;
		BinarySearchTree::Node* p;
		if (parent->right==node){
			q = parent->right->left;
			p = parent->right;
			parent->right = q;
		}
		else{
			q = parent->left->left;
			p = parent->left;
			parent->left = q;
		}	
		p->left = q->right;
		q->right = p;
	}
	
}

void AVLTree::singleLeft(BinarySearchTree::Node* node, BinarySearchTree::Node* parent){
	if (!node || !parent)
		return;
	if (BinarySearchTree::getRootNode() == node){
		BinarySearchTree::Node* tempright = BinarySearchTree::getRootNode()->right;
		BinarySearchTree::Node* temp = BinarySearchTree::getRootNode()->right->left;
		BinarySearchTree::getRootNode()->right->left = BinarySearchTree::getRootNode();
		BinarySearchTree::getRootNode()->right = temp;
		*BinarySearchTree::getRootNodeAddress() = tempright;
	}
	else
	{
		BinarySearchTree::Node* q;
		BinarySearchTree::Node* p;
		if (parent->right==node){
			q = parent->right->right;
			p = parent->right;
			parent->right = q;
		}
		else{
			q = parent->left->right;
			p = parent->left;
			parent->left = q;
		}	
		p->right = q->left;
		q->left = p;
	}
}

void AVLTree::doubleLR (BinarySearchTree::Node* node, BinarySearchTree::Node* parent){
	singleLeft(node->left, node);
	singleRight(node, parent);
}

void AVLTree::doubleRL (BinarySearchTree::Node* node, BinarySearchTree::Node* parent){
	singleRight(node->right, node);
	singleLeft(node, parent);
}

void AVLTree::traverseUpdate(BinarySearchTree::Node* node){
	if (node == BinarySearchTree::getRootNode() && BinarySearchTree::getRootNode()->left && BinarySearchTree::getRootNode()->right)
	{
		if (getDepth(node->right) == 0 && (node->left->avlBalance == -1 || node->left->avlBalance == 1))
			node->avlBalance = getDepth(node->left) - getDepth(node->right) - 1;
		else if (getDepth(node->left) == 0 && node->right->avlBalance == -1 || node->right->avlBalance == 1 )
			node->avlBalance = getDepth(node->left) - getDepth(node->right) + 1;
		else
			node->avlBalance = getDepth(node->left) - getDepth(node->right);
	}
	else
		node->avlBalance = getDepth(node->left) - getDepth(node->right);
	if (node->left)
		traverseUpdate(node->left);
	if (node->right)
		traverseUpdate(node->right);
}

void AVLTree::updateNodeBalance(){
	if (!getRootNode())
		return;
	traverseUpdate(BinarySearchTree::getRootNode());
}
