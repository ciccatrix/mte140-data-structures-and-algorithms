//Kanvardeep Rakhraj 20622622
//Sally Hui 20608550

#include <iostream>
#include "a3_binary_search_tree.hpp"

using namespace std;

typedef int DataType;

BinarySearchTree::Node::Node(DataType newval)
{
	val = newval;
	left = NULL;
	right = NULL;
	avlBalance=0;	
}

BinarySearchTree::BinarySearchTree(){
	root_ = NULL;
	size_ = 0;
}

BinarySearchTree::~BinarySearchTree(){
	
}
  
int BinarySearchTree::getNodeDepth(Node* n) const{
	int ctr = 0;
	bool end = false;
	Node* node = root_;
	while (node != n && node){
		if (n->val < node->val)
			node =  n->left;
		else if (n->val >= node->val)
			node = n->right;
		ctr++;
	}
	return ctr;
}
  
unsigned int BinarySearchTree::size() const{
	return size_;
}

DataType BinarySearchTree::max() const{
	if (size_==0)
		return NULL;
	
	else{
		Node* node = root_;
		while (node->right){
			node = node->right;
		}
		return node->val;
	}
}

DataType BinarySearchTree::min() const{
	if (size_==0)
		return NULL;
	
	else{
		Node* node = root_;
		while (node->left){
			node = node->left;
		}
		return node->val;
	}
}

int getDepth(BinarySearchTree::Node* node){
	if (!node)
		return -1;
	else
		return 1+max(getDepth(node->left),getDepth(node->right));
}

unsigned int BinarySearchTree::depth() const{
	return getDepth(root_);
}

void BinarySearchTree::print() const{
}

bool BinarySearchTree::exists(DataType val) const{
	if (!root_)
		return false; 
	Node* node = root_;
	while (node !=NULL)
	{
		if (node-> val == val)
			return true;
		else if (node->val < val){
			node = node->right;
		}
		else if (node->val > val){
			node = node->left;
		}
	}
	return false;
}

BinarySearchTree::Node* BinarySearchTree::getRootNode(){
	return root_;
}

BinarySearchTree::Node** BinarySearchTree::getRootNodeAddress(){
	return &root_;
}


bool BinarySearchTree::insert(DataType val){
	if (!root_)
	{
		root_ = new Node (val);
		size_++;
		return true;
	}
	else if (exists(val))
		return false;
	else {
		 Node* node = root_;
		 Node* backNode = root_;
		 while(node){
		 	if (node != backNode)
		 		backNode = node;
		 		
		 	if(val<node->val){
		 		node = node->left;
		 	}
		 	else 
		 		node = node->right;
		 }
		 if (val < backNode->val)
		 	backNode->left = new Node(val);
		 else
		 	backNode->right = new Node(val);
		 	
		size_++;
		return true;
	}
}

bool BinarySearchTree::remove(DataType val){
	if (exists(val) == false)
		return false;
	if (size_==0)
		return false;
	else if (size_==1){
		root_=NULL;
		size_--;
		return true;
	}
//	else if (root_->val == val)
//		root_ = NULL;
	else {
		Node* backNode= root_;
		Node* node = root_;
		bool end = false;
		while (node && !end){
			if (node != backNode && node->val != val)
				backNode = node;
			if (node->val == val)
				end = true;
			else if (node->val < val)
				node = node->right;
			else
				node = node->left;
				
		}
		if (!node->right && !node->left){ // LEAF NODE
			if (val<backNode->val) 
				backNode->left = NULL;
			else 
				backNode->right = NULL;
		}
		else if (node->left && node->right){
			Node* successor = node->right;
			while (successor->left){
				if (successor != backNode)
					backNode = successor;
				successor = successor->left;
			}
			node->val = successor->val;
			if (successor==node->right){
				node->right = node->right->right;
				size_--;
				return true;
			}
			else
				backNode->left = NULL;
		}
		else{
			if (val==root_->val){
				if(node->right)
					node=node->right;
				else if (node->left)
					node=node->left;
			}
			else if (backNode->left == node){
				if (node->right)
					backNode->left =backNode->left->right;
				else
					backNode->left=backNode->left->left;
			}
			else{
				if (node->right)
					backNode->right = backNode->right->right;
				else
					backNode->right = backNode->right->left;
			}
		}
		size_--;
		return true;
	}
}

void BinarySearchTree::updateNodeBalance(Node* n){
}


/*CONSOLE OUTPUT
PRIORITY QUEUE TESTING RESULTS
******************************
Test1: New queue is valid
TEST PASSED
Test2: Enqueue one item and then dequeue it
TEST PASSED
Test3: Enqueue too many
TEST PASSED
Test4: Enqueue too many then dequeue too many
TEST PASSED
Test5: Dequeue 5, enqueue 10, with repeat
TEST PASSED
Test6: Lots of enqueues and dequeues
TEST PASSED

BINARY SEARCH TREE TESTING RESULTS
**********************************
Test1: New tree is valid
TEST PASSED
Test2: Test a tree with one node
TEST PASSED
Test3: Insert, remove, and size on linear list formation with three elements
TEST PASSED
Test4: Test removal of a node with one child
TEST PASSED
Test5: Insert multiple elements and remove till nothing remains
TEST PASSED
Test6: Test removal of root node when both children of root have two children
TEST PASSED
Test7: Test depth with many inserts & some removes
TEST PASSED
Test8: Lots of inserts and removes
TEST PASSED

AVL TREE TESTING RESULTS
************************
Test1: Test single left rotation
TEST PASSED
Test2: Test single right rotation
TEST PASSED
Test3: Test double left-right rotation
TEST PASSED
Test4: Test double right-left rotation
TEST PASSED
Test5: Test multiple rotations on insert
TEST PASSED
Test6: Test multiple rotations on remove
TEST PASSED

Press any key to continue . . .

*/


