//Kanvardeep Rakhraj 20622622
//Sally Hui 20608550

#include <iostream>
#include "a3_priority_queue.hpp"

typedef int DataType;

using namespace std;

PriorityQueue::PriorityQueue(unsigned int capacity){
	heap_ = new DataType[capacity+1];
	size_=0;
	capacity_=capacity;
}

PriorityQueue::~PriorityQueue(){
	delete [] heap_;
	heap_ = NULL;
}

bool PriorityQueue::empty() const{
	return size_ == 0;
}

unsigned int PriorityQueue::size() const{
	return size_;
}

bool PriorityQueue::full() const{
	return size_ == capacity_;
}

void PriorityQueue::print() const{
}

DataType PriorityQueue::max() const{
	return heap_[1];
}

bool PriorityQueue::enqueue(DataType val){
	if (empty()){
		heap_[1] = val;
		size_++;
		return true;
	}
	else if (full()){
		return false;
	}
	else{
		heap_[size_ + 1] = val;
		int index = size_ + 1;
		bool end = false;
		while (index >= 2 && !end)
		{
			int parent = heap_[index / 2];
			if (parent < heap_[index])
			{
				heap_[index/2] = heap_[index];
				heap_[index] = parent;
			}
			else
				end = true;
				
			index = index / 2;
		} 
		size_++;
		return true;
	}
}
bool PriorityQueue::dequeue(){
	if (empty())
		return false;
		
	else if (size()==1){
		size_--;
		return true;	
	}
	
	else{
		heap_[1]=heap_[size_];
		size_--;
		
		int index = 1;
		bool end = false;
		while(index<=size_/2 && !end){
			int parent = heap_[index];
			int highchild;
			if (heap_[2*index]>heap_[2*index+1])
				highchild = heap_[2*index];
			else
				highchild = heap_[2*index+1];
				
			if (parent<highchild){
				if(highchild == heap_[2*index]){
					heap_[index] = highchild;
					heap_[2*index] = parent;
					index*=2;	
				}
				else if (highchild == heap_[2*index+1]){
					heap_[index] = heap_[2*index+1];
					heap_[2*index+1] = parent;
					index = 2*index+1;
				}
			}
			else
				end = true;
		
		}
		return true;
	}
}
