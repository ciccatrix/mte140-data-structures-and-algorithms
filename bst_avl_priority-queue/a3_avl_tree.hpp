#ifndef A3_AVL_TREE_HPP
#define A3_AVL_TREE_HPP

#include "a3_binary_search_tree.hpp"
#include <cmath>

using namespace std;

typedef int DataType;

class AVLTree: public BinarySearchTree
{
public:
	// Overriden insert and remove functions 
	// Do not modify these definitions
    bool insert(DataType val);
    bool remove(DataType val);

	// Define additional functions and attributes that you need below
	int getDepth(BinarySearchTree::Node* node);
	void balanceAVLTree(BinarySearchTree::DataType val);
	void updateNodeBalance();
	void traverseUpdate(BinarySearchTree::Node* node);
	void singleRight(BinarySearchTree::Node* node, BinarySearchTree::Node* parent);
    void singleLeft(BinarySearchTree::Node* node, BinarySearchTree::Node* parent);
    void doubleLR (BinarySearchTree::Node* node, BinarySearchTree::Node* parent);
    void doubleLR (BinarySearchTree::Node* node);
	void doubleRL (BinarySearchTree::Node* node, BinarySearchTree::Node* parent);
	BinarySearchTree::Node* getParent(Node* node);
	int getNodeDepth(BinarySearchTree::Node* n);
};
#endif

