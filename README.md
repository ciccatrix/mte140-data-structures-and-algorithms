# MTE140 - Data Structures and Algorithms

Implementations of various data structures and algorithms, including Binary Search Tree, AVL Tree, Circular Queue, Dynamic Stack, and Doubly Linked List.