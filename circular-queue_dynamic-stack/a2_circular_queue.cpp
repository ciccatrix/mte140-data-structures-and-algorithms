#include <iostream>
#include "a2_circular_queue.hpp"

using namespace std;

typedef CircularQueue::QueueItem QueueItem;
const QueueItem CircularQueue::EMPTY_QUEUE = -999;

/*
DYNAMIC STACK TESTING RESULTS
*****************************
Test1: New stack is valid
TEST PASSED
Test2: Push one item and then pop it
TEST PASSED
Test3: Simple push, pop, and peek test
TEST PASSED
Test4: Push 4 items, peek 5 items and then pop 5 items
TEST PASSED
Test5: Full stack doubles properly
TEST PASSED
Test6: Full stack doubles, then full stack doubles
TEST PASSED
Test7: Push a lot, pop a lot, no halving
TEST PASSED
Test8: Push a lot, pop a lot, halving
TEST PASSED
Test9: Lots of pushes, peeks, and pops
TEST PASSED
Test10: Lots of pushes, peeks, and pops
TEST PASSED

CIRCULAR QUEUE TESTING RESULTS
******************************
Test1: New queue is valid
TEST PASSED
Test2: Enqueue one item and then dequeue it
TEST PASSED
Test3: Enqueue two items and then dequeue three
TEST PASSED
Test4: Enqueue 16 items, peek 3 items and then dequeue 18 items
TEST PASSED
Test5: Enqueue items onto a full queue returns false
TEST PASSED
Test6: Queue is circular for enqueue()
TEST PASSED
Test7: Fill queue, then empty queue
TEST PASSED
Test8: Queue is circular for dequeue()
TEST PASSED
Test9: Lots of enqueues, dequeues, peeks, all valid
TEST PASSED
Test10: Lots of enqueues, dequeues, peeks, not all valid
TEST PASSED

Press any key to continue . . .
*/

CircularQueue::CircularQueue()
{
    items_ = new QueueItem [16];
    capacity_ = 16;
    size_ = 0;
    head_ = 0;
    tail_ = 1;
}

CircularQueue::CircularQueue(unsigned int capacity)
{
	items_ = new QueueItem [capacity];
    capacity_ = capacity;
    size_ = 0;
    head_ = 0;
    tail_ = capacity;
}

CircularQueue::~CircularQueue()
{
	delete []items_;
}

bool CircularQueue::empty() const
{    
	return size_ == 0;
}

bool CircularQueue::full() const
{
	return size_ == capacity_;
}

int CircularQueue::size() const
{  
	return size_;
}

bool CircularQueue::enqueue(QueueItem value)
{
	if(size_ == capacity_)
		return false;
	
	else if (size_ == 0)
	{
		items_[tail_-1] = value;
		size_++;
		return true;
	}
	else
	{
		items_[tail_]=value;
		tail_ = (tail_ + 1)%capacity_;
		size_++;
		return true;
	}
}

QueueItem CircularQueue::dequeue()
{
	if (empty())
		return EMPTY_QUEUE;
	else
	{
		int retVal = items_[head_];
		size_--;
		head_ = (head_ + 1)%capacity_;
		return retVal;
	}
}

QueueItem CircularQueue::peek() const
{
	if (empty())
		return EMPTY_QUEUE;
	else
		return items_[head_];
}

void CircularQueue::print() const
{
	for (int i=head_; i<head_+size_; i++)
		cout<<items_[i]<<endl;
}

