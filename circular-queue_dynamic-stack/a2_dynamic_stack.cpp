// SALLY HUI and YOUSSEF Roman
#include <iostream>
#include "a2_dynamic_stack.hpp"

using namespace std;

/*
DYNAMIC STACK TESTING RESULTS
*****************************
Test1: New stack is valid
TEST PASSED
Test2: Push one item and then pop it
TEST PASSED
Test3: Simple push, pop, and peek test
TEST PASSED
Test4: Push 4 items, peek 5 items and then pop 5 items
TEST PASSED
Test5: Full stack doubles properly
TEST PASSED
Test6: Full stack doubles, then full stack doubles
TEST PASSED
Test7: Push a lot, pop a lot, no halving
TEST PASSED
Test8: Push a lot, pop a lot, halving
TEST PASSED
Test9: Lots of pushes, peeks, and pops
TEST PASSED
Test10: Lots of pushes, peeks, and pops
TEST PASSED

CIRCULAR QUEUE TESTING RESULTS
******************************
Test1: New queue is valid
TEST PASSED
Test2: Enqueue one item and then dequeue it
TEST PASSED
Test3: Enqueue two items and then dequeue three
TEST PASSED
Test4: Enqueue 16 items, peek 3 items and then dequeue 18 items
TEST PASSED
Test5: Enqueue items onto a full queue returns false
TEST PASSED
Test6: Queue is circular for enqueue()
TEST PASSED
Test7: Fill queue, then empty queue
TEST PASSED
Test8: Queue is circular for dequeue()
TEST PASSED
Test9: Lots of enqueues, dequeues, peeks, all valid
TEST PASSED
Test10: Lots of enqueues, dequeues, peeks, not all valid
TEST PASSED

Press any key to continue . . .
*/

typedef DynamicStack::StackItem StackItem;  // for simplicity
const StackItem DynamicStack::EMPTY_STACK = -999;

DynamicStack::DynamicStack()
{
	items_ = new StackItem[16];
	size_ = 0;
	capacity_ = 16;
	init_capacity_ = 16;
}

DynamicStack::DynamicStack(unsigned int capacity)
{
	items_ = new StackItem[capacity];
	size_ = 0;
	capacity_ = capacity;
	init_capacity_ = capacity;
}

DynamicStack::~DynamicStack()
{
	delete [] items_;
}

bool DynamicStack::empty() const
{
	return size_ == 0;
}

int DynamicStack::size() const
{
	return size_;
}

void DynamicStack::push(StackItem value)
{
	if(size_==capacity_){
		StackItem* newItems_ = new StackItem[capacity_*2];
		for (int i = 0; i < size_; i++)
			newItems_[i] = items_[i];
		delete [] items_;
		items_ = newItems_;
		capacity_*=2;
		items_[size_]=value;
		size_++;
}
	else{
		items_[size_]=value;
		size_++;
	}
}
StackItem DynamicStack::pop()
{
	if (!empty() && capacity_ > 0)
	{
		StackItem retValue = items_[size_-1];
		size_--;
		
		if (size_ <= capacity_/4 && capacity_/2 >= init_capacity_)
		{
			StackItem* newItems_ = new StackItem[capacity_/2];
			for (int i = 0; i < size_; i++)
				newItems_[i] = items_[i];
			delete [] items_;
			items_ = newItems_;
			capacity_=capacity_/2;
		} 
		return retValue;
	}
	else
		return EMPTY_STACK;
}

StackItem DynamicStack::peek() const
{
	if (!empty() && capacity_ > 0)
	{
		StackItem retValue = items_[size_-1];
		return retValue;
	}
	else
		return EMPTY_STACK;
}

void DynamicStack::print() const
{
	for( int i=0; i<size_; i++)
	{
		cout<<items_[i]<<endl;
	}
}

