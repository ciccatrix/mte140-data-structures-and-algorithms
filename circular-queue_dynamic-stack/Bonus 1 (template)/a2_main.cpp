// SALLY HUI AND YOUSSEF ROMAN
#include <iostream>
#include <string>
using namespace std;

#include "a2_tests.hpp"
#include "a2_circular_queue_template.hpp"

/*
CIRCULAR QUEUE (template)TESTING RESULTS
******************************
Test1: Enqueue two items and then dequeue three for integer
TEST PASSED

Test2: Enqueue two items and then dequeue three for Double
TEST PASSED

Test3: Enqueue two items and then dequeue three for String
TEST PASSED

Press any key to continue . . .

*/
string get_status_str(bool status)
{
    return status ? "TEST PASSED" : "TEST FAILED";
}

int main() {
    CircularQueueTest queue_test;

	string queue_descriptions[3]={
      "Test1: Enqueue two items and then dequeue three for integer",
	  "Test2: Enqueue two items and then dequeue three for Double",
	  "Test3: Enqueue two items and then dequeue three for String"};
	bool queue_test_results [3]= {
	queue_test.test1(),
	queue_test.test2(),
	queue_test.test3()};
	

	cout << "CIRCULAR QUEUE (template)TESTING RESULTS \n";
	cout << "****************************** \n";
	for(int i=0; i<3; i++){
    	cout << queue_descriptions[i] << endl << get_status_str(queue_test_results[i])<< endl;
	cout << endl;
	}
	system("pause");
}
