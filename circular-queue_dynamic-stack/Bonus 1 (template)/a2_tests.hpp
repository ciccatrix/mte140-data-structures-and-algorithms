// SALLY HUI AND YOUSSEF ROMAN
#ifndef A2_TESTS_HPP
#define A2_TESTS_HPP

#define ASSERT_TRUE(T) if (!(T)) return false;
#define ASSERT_FALSE(T) if ((T)) return false;

#include "a2_circular_queue_template.hpp"

#include <iostream>

using namespace std;

class CircularQueueTest
{
public:
	
	// Enqueue two items and then dequeue three
    bool test1() {
	      CircularQueue<int> queue;
	      ASSERT_TRUE(queue.enqueue(10) == true)
	      ASSERT_TRUE(queue.enqueue(20) == true)     
	      ASSERT_TRUE(queue.empty() == false)
	      ASSERT_TRUE(queue.full() == false)
	      ASSERT_TRUE(queue.size_ == 2)
	      ASSERT_TRUE(queue.head_ == 0)
	      ASSERT_TRUE((queue.tail_ == 2) || ((queue.tail_ == 3)))
	      ASSERT_TRUE(queue.peek() == 10);  
	      ASSERT_TRUE(queue.dequeue() == 10);
	      ASSERT_TRUE(queue.peek() == 20);  
	      ASSERT_TRUE(queue.dequeue() == 20);  
	      ASSERT_TRUE(queue.empty() == true)
	      ASSERT_TRUE(queue.size_ == 0)
	      ASSERT_TRUE(queue.head_ == 2)
	      ASSERT_TRUE((queue.tail_ == 2) || (queue.tail_ == 3))
	      return true;
    }
    bool test2() {
	      CircularQueue<double> queue;
	      ASSERT_TRUE(queue.enqueue(10) == true)
	      ASSERT_TRUE(queue.enqueue(20) == true)     
	      ASSERT_TRUE(queue.empty() == false)
	      ASSERT_TRUE(queue.full() == false)
	      ASSERT_TRUE(queue.size_ == 2)
	      ASSERT_TRUE(queue.head_ == 0)
	      ASSERT_TRUE((queue.tail_ == 2) || ((queue.tail_ == 3)))
	      ASSERT_TRUE(queue.peek() == 10);  
	      ASSERT_TRUE(queue.dequeue() == 10);
	      ASSERT_TRUE(queue.peek() == 20);  
	      ASSERT_TRUE(queue.dequeue() == 20);  
	      ASSERT_TRUE(queue.empty() == true)
	      ASSERT_TRUE(queue.size_ == 0)
	      ASSERT_TRUE(queue.head_ == 2)
	      ASSERT_TRUE((queue.tail_ == 2) || (queue.tail_ == 3))
	      return true;
    }
     bool test3() {
	      CircularQueue<string> queue;
	      ASSERT_TRUE(queue.enqueue("Strawberry") == true)
	      ASSERT_TRUE(queue.enqueue("Banana") == true)     
	      ASSERT_TRUE(queue.empty() == false)
	      ASSERT_TRUE(queue.full() == false)
	      ASSERT_TRUE(queue.size_ == 2)
	      ASSERT_TRUE(queue.head_ == 0)
	      ASSERT_TRUE((queue.tail_ == 2) || ((queue.tail_ == 3)))
	      ASSERT_TRUE(queue.peek() == "Strawberry");  
	      ASSERT_TRUE(queue.dequeue() == "Strawberry");
	      ASSERT_TRUE(queue.peek() == "Banana");  
	      ASSERT_TRUE(queue.dequeue() == "Banana");  
	      ASSERT_TRUE(queue.empty() == true)
	      ASSERT_TRUE(queue.size_ == 0)
	      ASSERT_TRUE(queue.head_ == 2)
	      ASSERT_TRUE((queue.tail_ == 2) || (queue.tail_ == 3))
	      return true;
    }
};
#endif
