// SALLY HUI AND YOUSSEF ROMAN
#include <iostream>
#include <typeinfo>
#include <string.h>
#include "a2_circular_queue_template.hpp"

using namespace std;

template<class Item>
Item CircularQueue<Item>::EMPTY_QUEUE;

template<class Item>
Item CircularQueue<Item>:: emptyVal() const
{
	return -999;
}

template<>
string CircularQueue<string>::emptyVal () const
{
	return "";
}

template<class Item>
CircularQueue<Item>::CircularQueue()
{
    items_ = new Item [16];
    capacity_ = 16;
    size_ = 0;
    head_ = 0;
    tail_ = 1;
    EMPTY_QUEUE = emptyVal();
}

template<class Item>
CircularQueue<Item>::CircularQueue(unsigned int capacity)
{
	items_ = new Item [capacity];
    capacity_ = capacity;
    size_ = 0;
    head_ = 0;
    tail_ = capacity;
    EMPTY_QUEUE = emptyVal();
}

template<class Item>
CircularQueue<Item>::~CircularQueue()
{
	delete []items_;
}

template<class Item>
bool CircularQueue<Item>::empty() const
{    
	return size_ == 0;
}

template<class Item>
bool CircularQueue<Item>::full() const
{
	return size_ == capacity_;
}

template<class Item>
int CircularQueue<Item>::size() const
{  
	return size_;
}

template<class Item>
bool CircularQueue<Item>::enqueue(Item value)
{
	if(size_ == capacity_)
		return false;
	
	else if (size_ == 0)
	{
		items_[tail_-1] = value;
		size_++;
		return true;
	}
	else
	{
		items_[tail_]=value;
		tail_ = (tail_ + 1)%capacity_;
		size_++;
		return true;
	}
}

template<class Item>
Item CircularQueue<Item>::dequeue()
{
	Item it;
	if (empty())
		return EMPTY_QUEUE;
	else
	{
		Item retVal = items_[head_];
		size_--;
		head_ = (head_ + 1)%capacity_;
		return retVal;
	}
}

template<class Item>
Item CircularQueue<Item>::peek() const
{
	Item it;
	if (empty())
		return EMPTY_QUEUE;
	else
		return items_[head_];
}

template<class Item>
void CircularQueue<Item>::print() const
{
	for (int i=head_; i<head_+size_; i++)
		cout<<items_[i]<<endl;
}

template class CircularQueue <int>;
template class CircularQueue <double>;
template class CircularQueue <string>;


