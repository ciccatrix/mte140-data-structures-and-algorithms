//Sally Hui
//Youssef Roman
#define ASSERT_TRUE(T) if (!(T)) return false;
#define ASSERT_FALSE(T) if ((T)) return false;

#include <iostream>
#include <vector>

using namespace std;

bool test1() {
	
	vector<int> v;
	ASSERT_TRUE(v.size() == 0); // size at initialization
	ASSERT_TRUE(v.capacity()== 0); // capacity at initialization
	
	v.reserve(5);
	ASSERT_TRUE(v.size() == 0); // size unchanged by reserve
	ASSERT_TRUE(v.capacity() == 5); // capacity set to 5
	
	for (int i = 0; i < 5; i++) // capacity does not change until capacity is filled
	{
		v.push_back(i);
		ASSERT_TRUE(v.capacity() == 5);
	}
	
	for (int i = 5; i < 10; i++) // after reserved capacity is exceeded, capacity is doubled
	{
		v.push_back(i);
		ASSERT_TRUE(v.capacity() == 10);
	}
	
	for (int i = 0; i < 10; i++) // use at function to check element values
		ASSERT_TRUE (v.at(i) == i);
		
	v.insert(v.begin() + 5, 420); // insert immediately before element at index 5
	ASSERT_TRUE(v.capacity() == 20); // check capacity doubled
	// expected: 0, 1, 2, 3, 4, 420, 5, 6, 7, 8, 9
	for (int i = 0; i < 5; i++) // check for expected values
		ASSERT_TRUE(v[i] == i);
	ASSERT_TRUE(v[5] == 420);
	for (int i = 6; i < 11; i++)
		ASSERT_TRUE(v[i] == i - 1);
		
	for (int i = 1; i < 12; i++) // check capacity doubles again after using insert to insert more values
		v.insert(v.begin(), i);
	ASSERT_TRUE(v.capacity()==40);

	return true;
}

bool test2() {
	vector<int> v;
	v.reserve(10);
	
	for (int i = 0; i < 10; i++) // simply assigning values like an array does not increment size
		v[i] = i;
	ASSERT_TRUE(v.size() == 0);
	
	for (int i = 10; i < 20; i++) 
		v.push_back(i);
		
	for (int i = 0; i < 10; i++) // since size didn't change, push_back overwrote the values
		ASSERT_TRUE(v.at(i) == i + 10);
	
	v.push_back(10); // check capacity doubles
	ASSERT_TRUE(v.capacity() == 20);
	
	for (int i = 0; i < 11; i++) // popping back does not change capacity
	{ 
		v.pop_back();
		ASSERT_TRUE(v.capacity() == 20);
	}
	
	v.push_back(10);
	v.push_back(20);
	ASSERT_TRUE(v.size() == 2);
	
	vector<int>::iterator forwardIt = v.begin();
	ASSERT_TRUE(*forwardIt == 10);
	
	v.erase(v.begin()); // erase first element of vector
	
	ASSERT_TRUE(*forwardIt == 20); // iterator now points at the originally second element, because erase shifted 20 backwards to index 0
	ASSERT_TRUE(v.size()==1);
	ASSERT_TRUE(v.capacity()==20);
	
	return true;
}

bool test3(){
	vector<int> v;
	v.resize(10);
	
	ASSERT_TRUE(v.size()==10); // resize changes both size and capacity to 10
	ASSERT_TRUE(v.capacity()==10);
	
	for (int i = 0 ; i < 10; i++)
		ASSERT_TRUE(v[i]==0); // resize by default allocates 0 to all elements
		
	vector<int>::iterator forwardIt = v.begin(); 
	for (int i = 0; i < 10; i++) // iterate through vector assigning values using pointer addition
	{
		forwardIt = v.begin() + i;
		*forwardIt = i;
	}
	
	forwardIt = v.begin(); // iterate through vector checking values only incrementing
	for (int i = 0; i < 10; i++)
	{
		ASSERT_TRUE(*forwardIt == i);
		forwardIt++;
	}
	
	vector<int>::iterator backwardIt = v.end(); // iterate through vector backwards by decrementing
	backwardIt--; // end points 1 after last element
	for (int i = 9; i >= 0; i--) 
	{
		ASSERT_TRUE(*backwardIt == i);
		backwardIt--;
	}
	
	backwardIt = v.end(); // iterate through vector backwards using pointer addition
	for (int i = 9; i >= 0; i--)
	{
		backwardIt = v.end() - 10 + i;
		ASSERT_TRUE(*backwardIt == i);
	}
	return true;
}
    
    
string get_status_str(bool status)
{
    return status ? "TEST PASSED" : "TEST FAILED";
}

int main() {
	string queue_descriptions[3]={
      "Test1: capacity and size after reserve, change in capacity afterceeding reserved capacity, assign vector member values using ush_back, use of at() to check values, insert inside vector",
	  "Test2: assigning vector values with [] does not change capacity, popping ack does not change capacity, erase does not change capacity",
	  "Test3: resize changes both capacity and size, check default resize initiated values, iterate forwards and backwards through arrays using begin() and end() through incrementing, decrementing, and pointer addition"};
	bool test_results [3]= {test1(), test2(),test3()};

	cout << "VECTOR DEMO RESULTS \n";
	cout << "******************* \n";
	for(int i=0; i<3; i++){
    	cout << queue_descriptions[i] << endl << get_status_str(test_results[i])<< endl;
	cout << endl;
	}
	system("pause");
}
/*
*******************
Test1: capacity and size after reserve, change in capacity afterceeding reserved capacity, assign vector
member values using ush_back, use of at() to check values, insert inside vector
TEST PASSED

Test2: assigning vector values with [] does not change capacity, popping back does not change capacity, erase does not change capacity
TEST PASSED

Test3: resize changes both capacity and size, check default resize initiated values, iterate forwards and backwards
through arrays using begin() and end() through incrementing, decrementing, and pointer addition
TEST PASSED

Press any key to continue . . .
*/
