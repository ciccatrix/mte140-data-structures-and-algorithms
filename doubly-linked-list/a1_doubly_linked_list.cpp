//Sally Hui 20608550
//Han Deng 20621915
#include "a1_doubly_linked_list.hpp"
#include <cstdlib>
#include <iostream>

using namespace std;
/*
DOUBLY LINKED LIST TESTING RESULTS
**********************************
Test1: New empty list is valid
TEST PASSED
Test2: insert_front() and insert_back() on zero-element list
TEST PASSED
Test3: select() and search() work properly
TEST PASSED
Test4: remove_front() and remove_back() on one-element list
TEST PASSED
Test5: replace() works properly
TEST PASSED
Test6: insert_front() keeps moving elements forward
TEST PASSED
Test7: inserting at different positions in the list
TEST PASSED
Test8: try to remove too many elements, then add a few elements
TEST PASSED
Test9: lots of inserts and deletes, all of them valid
TEST PASSED
Test10: lots of inserts and deletes, some of them invalid
TEST PASSED
*/
DoublyLinkedList::Node::Node(DataType data)
{
	value = data;
	prev = NULL;
	next = NULL;
}

DoublyLinkedList::DoublyLinkedList()
{
	head_ = NULL;
	tail_ = NULL;
	size_ = 0;
}

DoublyLinkedList::~DoublyLinkedList()
{
	Node* currNode = head_;
	for (int i = 0; i < size_-1; i++)
	{
		currNode = currNode -> next;
		delete currNode -> prev;
	}
	
	delete tail_;
}

unsigned int DoublyLinkedList::size() const
{
	return size_;
}

unsigned int DoublyLinkedList::capacity() const
{
	return CAPACITY;
}

bool DoublyLinkedList::empty() const
{
	return size_ == 0;
}

bool DoublyLinkedList::full() const
{
	return size_ == CAPACITY;
}

DoublyLinkedList::DataType DoublyLinkedList::select(unsigned int index) const
{
	return getNode(index)->value;
}

unsigned int DoublyLinkedList::search(DataType value) const
{
	int foundIndex = size_;
	Node* currNode = head_;

	for (int i = 0; i < size_ && foundIndex == size_; i++)
	{
		if (currNode->value == value)
			foundIndex = i;
			
		currNode = currNode->next;
	}
	
	return foundIndex;
}

void DoublyLinkedList::print() const
{
	cout<< endl;
	for (int i = 0; i < size_; i++)
		cout << getNode(i) -> value << " ";
	cout << endl;
}

DoublyLinkedList::Node* DoublyLinkedList::getNode(unsigned int index) const
{
	Node* findNode = tail_;

	if (index <= (size_ - 1) && index < CAPACITY && head_)
	{
		findNode = head_;
		for(int i = 0; i < index; i++){
			findNode = findNode -> next;
		}

	}

	return findNode;
}

bool DoublyLinkedList::insert(DataType value, unsigned int index)
{
	if(empty() || index == 0){
		return insert_front(value);
	}
	
	else if (index < size_ && index < CAPACITY && index >= 0)
	{
		Node* indexPrev = getNode(index) -> prev;
		Node* indexOld = getNode(index);
		Node* newNode = new Node(value);
		
		indexOld -> prev = newNode;
		indexPrev -> next = newNode;
		
		newNode -> prev = indexPrev;
		newNode -> next = indexOld;
		
		size_++;
		
		return true;
	}
	
	else if (index == size_){
		return insert_back(value);	
	}
	
	return false;
}

bool DoublyLinkedList::insert_front(DataType value)
{
	if(empty()){
		Node* newNode = new Node(value);
		head_= newNode;
		tail_= newNode;
	
		size_++;	
		return true;
	}
	else if(!full()){
		Node* temp = head_;
		Node* newNode = new Node(value);
		head_ = newNode;
		newNode->next = temp;
		temp -> prev = newNode;
		head_-> prev = NULL;
		
		size_++;
		return true;
	}
	else
		return false;
}

bool DoublyLinkedList::insert_back(DataType value)
{
	if(empty()){
		Node* newNode = new Node(value);
		head_ = newNode;
		tail_ = newNode;
		size_++;
		
		return true;
	}
	else if(!full()){
		Node* temp = tail_;
		Node* newNode = new Node(value);
		tail_ = newNode;
		temp -> next = newNode;
		newNode->prev = temp;
		tail_->next = NULL;
		
		size_++;
		
		return true;
	}
	else
		return false;
	
}

bool DoublyLinkedList::remove(unsigned int index)
{
		if(index == 0 && !empty())
		{
			return remove_front();
		}
		
		else if(index == size_ -1 && !empty())
		{
			return remove_back();
		}
		
		else if(index < size_ -1 && index < CAPACITY && !empty()){
			Node* tbrprev = getNode(index -1);
			Node* tbrnext = getNode(index +1);
			tbrprev->next = tbrnext;
			tbrnext->prev = tbrprev;
			
			delete getNode(index);
			return true;
		}
		
		else{
			return false;
		}
}

bool DoublyLinkedList::remove_front()
{
	if(empty()){
		return false;
	}
	
	else if(size_ == 1){
		delete head_;
		head_ = NULL;
		tail_ = NULL;
		size_--;
		
		return true;
	}
	else{
		Node* temp = head_ -> next;
		delete head_;
		head_ = temp;
		head_ -> prev = NULL;
		size_--;
		
		return true;
	}
}

bool DoublyLinkedList::remove_back()
{
	if(empty()){
		return false;
	}
	
	else if(size_ == 1){
		delete tail_;
		head_ = NULL;
		tail_ = NULL;
		size_--;
		
		return true;
	}
	
	else{
		Node* temp = tail_ -> prev;
		delete tail_;
		tail_ = temp;
		tail_ -> next = NULL; 
		size_--;
		
		return true;
	}
}

bool DoublyLinkedList::replace(unsigned int index, DataType value)
{
	if (index < size_ && index < CAPACITY && index >= 0)
	{
		getNode(index) -> value = value;
		return true;
	}
	return false;
}

