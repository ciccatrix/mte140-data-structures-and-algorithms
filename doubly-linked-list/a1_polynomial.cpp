#include "a1_polynomial.hpp"
#include <cstdlib>
#include <iostream>

using namespace std;

// TEST RESULTS
/*
POLYNOMIAL TESTING RESULTS
*******************************
Test1: Constructor works as expected
TEST PASSED
Test2: Adding polynomials works as expected
TEST PASSED
Test3: Subtracting polynomials works as expected
TEST PASSED
Test4: Multiplying polynomials works as expected
TEST PASSED

Press any key to continue . . .
*/

Polynomial::Polynomial(int P[], int size){
	
		list = new DoublyLinkedList ();
	
		if(size < 0)
			size *= -1;
	
		for (int i = 0; i < size; i++)
			list->insert_back(P[i]);

}

Polynomial::~Polynomial(){
	delete list;
}

Polynomial* Polynomial::add(Polynomial* rhs){
	
	if (!list->empty() || !rhs->list->empty())
	{
		Polynomial* left = &(*this);
		Polynomial* right = rhs;
		int lSize = left->size();
		int rSize = right->size();
		
		int sSize = right->size(); // small size
		int gSize = left->size();
		
		if (lSize < rSize)
		{
			Polynomial* temp = right;
			right = left;
			left = temp;
			sSize = lSize;
			gSize = rSize;
		}
		
		int* coeff = new int [gSize];
		
		for(int i = 0; i < sSize; i++){
			coeff[i] = left->list->getNode(i)->value + right->list->getNode(i)->value;
		}
		
		for (int i = sSize; i < gSize; i++){
			coeff[i] = left->list->getNode(i)->value;
		}
		
		Polynomial* sum = new Polynomial(coeff, gSize);
		
		delete [] coeff;
		
		return sum;
	}
	else
	{
		return NULL;//designer's choice
	}
	
}

Polynomial* Polynomial::sub(Polynomial* rhs){
	if (!list->empty() || !rhs->list->empty())
	{
		Polynomial* left = &(*this);
		Polynomial* right = rhs;
		int lSize = left->size();
		int rSize = right->size();
		int gSize = lSize;
		
		if(rSize > lSize)
			gSize = rSize;
			
		int* coeff = new int [gSize];
		
		for(int i = 0; i < lSize; i++){
			coeff[i] = left->list->getNode(i)->value;
		}
		
		for(int i = lSize; i < gSize; i++){
			coeff[i] = 0;
		}
		
		for(int i = 0; i < rSize; i++){
			coeff[i] -= right->list->getNode(i)->value;
		}
		
		Polynomial* sub = new Polynomial(coeff, gSize);
		
		delete [] coeff;
		
		return sub;
	}
	else
	{
		return NULL;//designer's choice
	}
}

Polynomial*	Polynomial::mul(Polynomial* rhs){
	// null lhs/rhs
	if (list->empty() || rhs->list->empty())
		return NULL;
	else
	{
		Polynomial* lhs = &(*this);
		
		// max exponent of new polynomial
		int resExp = lhs->list->size() + rhs->list->size() - 1;
		
		// compute new polynomial
		int *res = new int [resExp];
		for (int i = 0; i < resExp; i++)
			res[i] = 0;
			
		for (int i = 0; i < lhs->list->size(); i++){
				for (int j = 0; j < rhs->list->size(); j++)
				{
					res[i + j] += lhs->list->getNode(i)->value * rhs->list->getNode(j)->value; 
				}
		}
		Polynomial* resPoly = new Polynomial (res, resExp);
		return resPoly;	
	}
	
	
}

int Polynomial::size(){
	return list->size();
}

void Polynomial::print(){
	for (int i = 0; i < size(); i++)
		cout << list->getNode(i)->value << "x^" << i << " ";
	cout << endl;
}
