#ifndef A1_POLYNOMIAL_HPP
#define A1_POLYNOMIAL_HPP

#include "a1_doubly_linked_list.hpp"

class Polynomial {
	private:
		DoublyLinkedList* list;
		friend class PolynomialTest;
		
	public:
		~Polynomial();
		Polynomial(int P[], int size);
		Polynomial* add(Polynomial* rhs);
		Polynomial* sub(Polynomial* rhs);
		Polynomial*	mul(Polynomial* rhs);
		int size();
		void print();
};

#endif 
