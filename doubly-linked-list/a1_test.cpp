#include "a1_polynomial.hpp"

#define ASSERT_TRUE(T) if (!(T)) return false;
#define ASSERT_FALSE(T) if ((T)) return false;

#include <cstdlib>
#include <iostream>

using namespace std;

class PolynomialTest{
	public:
		// Test constructor
		bool test1(){
			int a0 [3] = {0, 1, 2};
			Polynomial p1 (a0, 6);
			Polynomial p2 (a0, -6);
			
			ASSERT_TRUE(p1.list->select(0) == 0 && p2.list->select(0) == 0)
			ASSERT_TRUE(p1.list->select(1) == 1 && p2.list->select(1) == 1)
			ASSERT_TRUE(p1.list->select(2) == 2 && p2.list->select(2) == 2)
			
			int a1[5] = {0};
			Polynomial p3 (a1, 5);
			ASSERT_TRUE(p3.list->select(3) == 0 && p3.list->select(2) == p3.list->select(3))
			
			return true;
		}
		
		bool test2(){
			int a1[3] = {1,2,3};
			int a2[4] = {2,3,4,5};
			Polynomial p1 (a1, 3);
			Polynomial p2 (a2, 4);
			
			Polynomial p3 = *p1.add(&p2);
			Polynomial p4 = *p2.add(&p1);
		
			ASSERT_TRUE(p3.list->select(0) == p4.list->select(0) && p4.list->select(0) == 3)
			ASSERT_TRUE(p3.list->select(1) == p4.list->select(1) && p4.list->select(1) == 5)
			ASSERT_TRUE(p3.list->select(2) == p4.list->select(2) && p4.list->select(2) == 7)
			ASSERT_TRUE(p3.list->select(3) == p4.list->select(3) && p4.list->select(3) == 5)
			
			Polynomial p5 = *p1.add(&p1);
			ASSERT_TRUE(p5.list->select(0) == 2)
			ASSERT_TRUE(p5.list->select(1) == 4)
			ASSERT_TRUE(p5.list->select(2) == 6)
		
			int a3[0];
			Polynomial p6(a3, 0);
			Polynomial p7 = *p6.add(&p1);
			ASSERT_TRUE(p7.list->select(0) == p1.list->select(0) && p7.list->select(0) == 1)
			
			int a4[5] = {-1, -2, -3, -4};
			Polynomial p8 (a4, 4);
			Polynomial p9 = *p1.add(&p8);
			ASSERT_TRUE(p9.list->select(0) == 0)
			ASSERT_TRUE(p9.list->select(1) == 0)
			ASSERT_TRUE(p9.list->select(2) == 0)
			ASSERT_TRUE(p9.list->select(3) == -4)
			
			return true;
		}
		
		bool test3(){
			int a1[3] = {1,2,3};
			int a2[4] = {2,3,4,5};
			Polynomial p1 (a1, 3);
			Polynomial p2 (a2, 4);
			
			Polynomial p3 = *p1.sub(&p2);
			Polynomial p4 = *p2.sub(&p1);
			ASSERT_TRUE(p3.list->select(0) == -1)
			ASSERT_TRUE(p3.list->select(1) == -1)
			ASSERT_TRUE(p3.list->select(2) == -1)
			ASSERT_TRUE(p3.list->select(3) == -5)
			
			ASSERT_TRUE(p4.list->select(0) == 1)
			ASSERT_TRUE(p4.list->select(1) == 1)
			ASSERT_TRUE(p4.list->select(2) == 1)
			ASSERT_TRUE(p4.list->select(3) == 5)
			
		
			Polynomial p5 = *p1.sub(&p1);
			ASSERT_TRUE(p5.list->select(0) == 0)
			ASSERT_TRUE(p5.list->select(1) == 0)
			ASSERT_TRUE(p5.list->select(2) == 0)
		
			int a3[0];
			Polynomial p6(a3, 0);
			Polynomial p7 = *p6.sub(&p1);
			
			ASSERT_TRUE(p7.list->select(0) == -1)
			ASSERT_TRUE(p7.list->select(1) == -2)
			ASSERT_TRUE(p7.list->select(2) == -3)
			
			return true;
		}
		
		bool test4(){
			int a1[3] = {3,0,-2};
			int a2[3] = {-1,-4,2};
			Polynomial p1 (a1, 3);
			Polynomial p2 (a2, 3);
			
			Polynomial p3 = *p1.mul(&p2);
			Polynomial p4 = *p2.mul(&p1);
			ASSERT_TRUE(p3.list->select(0) == p4.list->select(0) && p4.list->select(0) == -3)
			ASSERT_TRUE(p3.list->select(1) == p4.list->select(1) && p4.list->select(1) == -12)
			ASSERT_TRUE(p3.list->select(2) == p4.list->select(2) && p4.list->select(2) == 8)
			ASSERT_TRUE(p3.list->select(3) == p4.list->select(3) && p4.list->select(3) == 8)
			ASSERT_TRUE(p3.list->select(4) == p4.list->select(4) && p4.list->select(4) == -4)
			
			Polynomial p5 = *p1.mul(&p1);
			ASSERT_TRUE(p5.list->select(0) == 9)
			ASSERT_TRUE(p5.list->select(1) == 0)
			ASSERT_TRUE(p5.list->select(2) == -12)
			ASSERT_TRUE(p5.list->select(3) == 0)
			ASSERT_TRUE(p5.list->select(4) == 4)
		
			int a3[5] = {0};
			Polynomial p6(a3, 5);
			Polynomial p7 = *p6.mul(&p1);
			ASSERT_TRUE(p7.list->select(0) == 0)
			ASSERT_TRUE(p7.list->select(1) == 0)
			ASSERT_TRUE(p7.list->select(2) == 0)
			ASSERT_TRUE(p7.list->select(3) == 0)
			ASSERT_TRUE(p7.list->select(4) == 0)
			ASSERT_TRUE(p7.list->select(5) == 0)
			ASSERT_TRUE(p7.list->select(6) == 0)
			// This implementation is not as accurate as it could be, but a design choice
			// was made to fulfil the requirements of the assignment.
			
			int a4[4] = {666, 420, 69, 1337};
			Polynomial p8 (a4, 4);
			Polynomial p9 = *p1.mul(&p8);
			ASSERT_TRUE(p9.list->select(0) == 1998)
			ASSERT_TRUE(p9.list->select(1) == 1260)
			ASSERT_TRUE(p9.list->select(2) == -1125)
			ASSERT_TRUE(p9.list->select(3) == 3171)
			ASSERT_TRUE(p9.list->select(4) == -138)
			ASSERT_TRUE(p9.list->select(5) == -2674)
			ASSERT_TRUE(p9.list->select(6) == -2674)
			
			return true;
		}
};

string get_status_str(bool status)
{
    return status ? "TEST PASSED" : "TEST FAILED";
}

int main() {
	PolynomialTest poly_test;

	string poly_test_descriptions[4] = {
      "Test1: Constructor works as expected",
      "Test2: Adding polynomials works as expected",
      "Test3: Subtracting polynomials works as expected",      
	  "Test4: Multiplying polynomials works as expected"
	};
	
	bool poly_test_results[4];
    poly_test_results[0] = poly_test.test1();
    poly_test_results[1] = poly_test.test2();
    poly_test_results[2] = poly_test.test3();
    poly_test_results[3] = poly_test.test4();

	cout << "POLYNOMIAL TESTING RESULTS \n";
	cout << "******************************* \n";
	for (int i = 0; i < 4; ++i) {
    	cout << poly_test_descriptions[i] << endl << get_status_str(poly_test_results[i]) << endl;
	}
	cout << endl;
    system("pause");
}
